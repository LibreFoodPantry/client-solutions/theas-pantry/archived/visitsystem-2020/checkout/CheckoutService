package com.checkout.rest_api;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CheckoutRestApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void checkApprovalFound() throws Exception {
		String id = "0491843";
		this.mockMvc
				.perform(get("http://localhost:8080/approved/" + id))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string(containsString("80.0")));
		id = "0712202";
		this.mockMvc
				.perform(get("http://localhost:8080/approved/" + id))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string(containsString("80.0")));
		id = "0576868";
		this.mockMvc
				.perform(get("http://localhost:8080/approved/" + id))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string(containsString("80.0")));
		id = "0617504";
		this.mockMvc
				.perform(get("http://localhost:8080/approved/" + id))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string(containsString("80.0")));
	}

	@Test
	public void checkApprovalNotFound() throws Exception {
		String id = "00000000";
		this.mockMvc
				.perform(get("http://localhost:8080/approved/" + id))
				.andDo(print())
				.andExpect(status().isNotFound())
				.andExpect(content().string(containsString("-1.0")));
	}

	@Test
	public void checkRegistrationFound() throws Exception {
		String id = "0491843";
		this.mockMvc
				.perform(get("http://localhost:8080/registered/" + id))
				.andDo(print())
				.andExpect(status().isAccepted())
				.andExpect(content().string("true"));
		id = "0712202";
		this.mockMvc
				.perform(get("http://localhost:8080/registered/" + id))
				.andDo(print())
				.andExpect(status().isAccepted())
				.andExpect(content().string("true"));
		id = "0576868";
		this.mockMvc
				.perform(get("http://localhost:8080/registered/" + id))
				.andDo(print())
				.andExpect(status().isAccepted())
				.andExpect(content().string("true"));
		id = "0617504";
		this.mockMvc
				.perform(get("http://localhost:8080/registered/" + id))
				.andDo(print())
				.andExpect(status().isAccepted())
				.andExpect(content().string("true"));
	}

	@Test
	public void checkRegistrationNotFound() throws Exception {
		String id = "00000000";
		this.mockMvc
				.perform(get("http://localhost:8080/registered/" + id))
				.andDo(print())
				.andExpect(status().isNotFound())
				.andExpect(content().string("false"));
	}
}