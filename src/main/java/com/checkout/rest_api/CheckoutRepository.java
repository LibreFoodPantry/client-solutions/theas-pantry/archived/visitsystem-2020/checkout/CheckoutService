package com.checkout.rest_api;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface CheckoutRepository extends MongoRepository<checkoutDb, String> {
}
