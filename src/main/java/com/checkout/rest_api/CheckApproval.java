package com.checkout.rest_api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CheckApproval {

    @GetMapping("/approved/{id}")
    public ResponseEntity<Double> isApproved(@PathVariable() String id) {
        double dummyWeight = 80;
        double notFound = -1.0;
        if (id.equals("0712202") ||
            id.equals("0491843") ||
            id.equals("0576868") ||
            id.equals("0617504")) {
            return new ResponseEntity<>(dummyWeight, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(notFound, HttpStatus.NOT_FOUND);
        }
    }
}
