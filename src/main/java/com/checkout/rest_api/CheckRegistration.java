package com.checkout.rest_api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CheckRegistration {

    @GetMapping("/registered/{id}")
    public ResponseEntity<Boolean> isRegistered(@PathVariable String id) {
        if (id.equals("0712202") ||
                id.equals("0491843") ||
                id.equals("0576868") ||
                id.equals("0617504")
        )
            return new ResponseEntity<>(true, HttpStatus.ACCEPTED);
        else return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
    }
}
