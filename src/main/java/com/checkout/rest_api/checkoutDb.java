package com.checkout.rest_api;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class checkoutDb {
    @Id
    private String id;
    private String userId;
    private double weight;
    private Date date;

    public checkoutDb(String id, String userId, double weight, Date date) {
        this.id = id;
        this.userId = userId;
        this.weight = weight;
        this.date = date;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }
    public void setUserID(String userId) {
        this.userId = userId;
    }
    public double getWeight() {
        return weight;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }

}
